import adapter.ISammelumrechnung;
import adapter.SammelumrechnungAdapter;
import decorator.EuroGebuehrenDecorator;
import observer.IObserver;
import observer.LogUmrechnungObserver;
import restlichemuster.*;

public class Main {
    public static void main(String[] args) {

        // Umrechner-Initialisierung mit Builder-Pattern
        WR mitbuilder = new EuroToDollar.EuroToDollarBuilder().mitFaktor(1.05).mitNaechster(new EuroToYen()).build();

        // Adapter um Adapter-Pattern zu testen
        ISammelumrechnung adapter = new SammelumrechnungAdapter(mitbuilder);

        // Observer um Observer-Pattern zu testen
        IObserver observer = new LogUmrechnungObserver();
        double[] betraege = {32, 12, 91, 431, 12};

        // Decorator um Decorator-Patter zu testen
        // WR decorated = new EuroGebuehrenDecorator(mitbuilder);

        // Observer anfügen
        mitbuilder.addObserver(observer);

        try {
            // Vergleich der 2 Rechner
            // System.out.println(mitbuilder.umrechnen("eur2dol", 14) + " Ohne Decorator");
            System.out.println(adapter.sammelumrechnen("eur2dol", betraege));
            System.out.println("Sammelumrechnung erfolgt.");
            // System.out.println(decorated.umrechnen("eur2dol", 10) + " Mit Decorator");
        } catch (KeinPassenderUmrechnerException e) {
            System.out.println("Kein passender Umrechner gefunden!");
        }

    }

}