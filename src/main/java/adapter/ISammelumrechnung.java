package adapter;

import restlichemuster.KeinPassenderUmrechnerException;

public interface ISammelumrechnung {

    public double sammelumrechnen(String variante, double[] betraege) throws KeinPassenderUmrechnerException;

}
