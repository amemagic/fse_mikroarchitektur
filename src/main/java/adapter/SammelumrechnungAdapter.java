package adapter;

import restlichemuster.KeinPassenderUmrechnerException;
import restlichemuster.WR;

public class SammelumrechnungAdapter implements ISammelumrechnung{

    public WR umrechner;

    public SammelumrechnungAdapter(WR umrechner){this.umrechner = umrechner;}

    public double sammelumrechnen(String variante, double[] betraege) throws KeinPassenderUmrechnerException {
        double summe = 0;

        for (double b:betraege) {
            summe += b;
        }

            return umrechner.umrechnen(variante, summe);
    }

}
