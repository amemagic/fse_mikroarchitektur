package observer;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

/**
 * Observer welcher die beobachteten Umrechnungen
 * in eine Log-Datei schreibt.
 */
public class LogUmrechnungObserver implements IObserver{

    /**
     * Schreibt die Umrechnungsdaten samt Zeitstempel in eine
     * log.txt Datei.
     * @param anfangsBetrag Betrag der Umrechnung
     * @param endBetrag Ergebnis der Umrechnung
     * @param variante Umrechnungsvariante
     */
    public void benachrichtigen(double anfangsBetrag, double endBetrag, String variante) {

        // Log-Eintrag als String
        String eintrag = "Datum: " + new Date() + " Anfangsbetrag: " + anfangsBetrag + " Umrechnung: " + variante + " Endbetrag: " + endBetrag;

        try {
            File datei = new File("log.txt");
            if(datei.createNewFile()) {
                // Datei erstellt
                try {
                    FileWriter myWriter = new FileWriter("log.txt");
                    myWriter.write(eintrag);
                    // System.out.println("Logdatei erstellt und erster Eintrag geschrieben."); // Debugging-Code
                    myWriter.close();
                } catch (IOException e) {
                    System.out.println("Fehler mit FileWriter aufgetreten.");
                    e.printStackTrace();
                }
            } else {
                // Datei existiert bereits, es wird in die bestehende Log-Datei geschrieben.
                try {
                    BufferedWriter myWriter = new BufferedWriter(new FileWriter("log.txt", true));
                    myWriter.newLine();
                    myWriter.write(eintrag);
                    // System.out.println("In Logdatei geschrieben."); // Debugging-Code
                    myWriter.close();
                } catch (IOException e) {
                    System.out.println("Fehler mit FileWriter aufgetreten.");
                    e.printStackTrace();
                }
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

}
