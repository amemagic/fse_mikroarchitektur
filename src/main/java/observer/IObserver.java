package observer;

/**
 * Interface für die Observer-Klassen.
 * Shreibt Funktionalität für das Benachritigen vor.
 */
public interface IObserver {

    void benachrichtigen(double anfangsBetrag, double endBetrag, String variante);

}
