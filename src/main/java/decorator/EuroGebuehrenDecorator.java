package decorator;
import restlichemuster.IUmrechnen;
import restlichemuster.KeinPassenderUmrechnerException;

public class EuroGebuehrenDecorator extends Decorator {

    /**
     * Decorator mit einer festen Gebühr von 5 Euro.
     * Wird nur angewendet wenn eine Umrechnung von Euro stattfindet.
     * @param naechster Das nächste Kettenelemente der COR.
     */
    public EuroGebuehrenDecorator(IUmrechnen naechster) {
        super(naechster);
    }

    /**
     * Holt sich den Umrechnungsbetrag über die nächsten
     * Kettenelemente und fügt eine Gebühr von 5 Euro hinzu
     * wenn eine Umrechnung von Euro stattfindet.
     * @param variante Umrechnungsvariante als String.
     * @param betrag Der zu umrechnende Betrag.
     * @return Der umgerechnete Betrag in der Zielwährung.
     */
    @Override
    public double umrechnen (String variante, double betrag) throws KeinPassenderUmrechnerException {
        if(variante.startsWith("eur")){
            return super.umrechnen(variante, betrag + 5);
        } else {
            return super.umrechnen(variante, betrag);
        }
    }

}
