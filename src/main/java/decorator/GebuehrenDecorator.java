package decorator;
import restlichemuster.IUmrechnen;
import restlichemuster.KeinPassenderUmrechnerException;

public class GebuehrenDecorator extends Decorator {

    /**
     * Decorator mit einer Gebühr von 5%.
     * Die 5% werden unabhängig von der Währung nach der Umrechnung
     * dazugezählt.
     * @param naechster Das nächste Kettenelement in der COR.
     */
    public GebuehrenDecorator(IUmrechnen naechster) {
        super(naechster);
    }

    /**
     * Holt sich den Umrechnungsbetrag über die nächsten
     * Kettenelemente und rechnet eine Gebühr von 5% hinzu.
     * @param variante Die Umrechnungsvariante als String.
     * @param betrag Der zu umrechnende Betrag in der Startwährung.
     * @return Der Umrechnungsbetrag inkl Gebühr.
     */
    @Override
    public double umrechnen (String variante, double betrag) throws KeinPassenderUmrechnerException {
        return super.umrechnen(variante, betrag) * 1.05;
    }

}
