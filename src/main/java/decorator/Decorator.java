package decorator;
import restlichemuster.IUmrechnen;
import restlichemuster.KeinPassenderUmrechnerException;
import restlichemuster.WR;

public abstract class Decorator extends WR {

    /**
     * Basis für die Decorator-Klassen.
     * Die geerbten WR-Methoden. werden einfach an das nächste
     * Kettenglied weitergeleitet.
     * @param naechster Das nächste Kettenelement für die COR.
     */
    public Decorator(IUmrechnen naechster) {super(naechster);}

    /**
     * Holt sich den Umrechnungsbetrag über das nächste
     * Kettenelement in der COR.
     * @param variante Die Umrechnungsvariante als String.
     * @param betrag Der zu umrechnende Betrag in der Startwährung.
     * @return Der umgerechnete Betrag in der Zielwährung.
     */
    @Override
    public double umrechnen (String variante, double betrag) throws KeinPassenderUmrechnerException {
        return getNaechster().umrechnen(variante, betrag); // COR-Pattern.
    }

    public double getFaktor(){return getNaechster().getFaktor();} // COR-Pattern.

    public boolean zustaendig(String variante){return getNaechster().zustaendig(variante);} // COR-Pattern.

}
