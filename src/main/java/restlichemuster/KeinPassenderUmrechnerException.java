package restlichemuster;

/**
 * Exception für den Fall, dass kein Element in der COR
 * für die gewünschte Umrechnung zuständig ist.
 */
public class KeinPassenderUmrechnerException extends Exception {
}
