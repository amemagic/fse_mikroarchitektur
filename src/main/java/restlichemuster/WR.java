package restlichemuster;
import observer.IObserver;
import java.util.ArrayList;
import java.util.List;

public abstract class WR implements IUmrechnen {

    private IUmrechnen naechster; // COR
    private List<IObserver> observerListe; // Observer-Pattern

    /**
     * Währungsrechner ohne nächstes Kettenelement.
     */
    public WR(){naechster = null; observerListe = new ArrayList<IObserver>();}

    /**
     * Währungsrechner mit nächstem Kettenelement.
     * @param naechster Das nächste Kettenelement für die Chain-Of-Responsibility.
     */
    public WR(IUmrechnen naechster){this.naechster = naechster; observerListe = new ArrayList<IObserver>();}

    /**
     * Umrechnung durch den Umrechnungsfaktor der jeweiligen Währung.
     * @param variante Die Variante als String. (Bsp.: EUR2DOL)
     * @param betrag Der umzurechnende Betrag.
     * @return Der umgerechnete Betrag in der Zielwährung.
     */
    public double umrechnen(String variante, double betrag) throws KeinPassenderUmrechnerException{
        if(zustaendig(variante)){
            double ergebnis = getFaktor() * betrag; //TemplateHook-Pattern

            // Auf zwei Stellen runden
            ergebnis = Math.round(ergebnis * 100);
            ergebnis = ergebnis / 100;

            alleBeobachterBenachrichtigen(betrag, variante, ergebnis); //Observer-Pattern

            return ergebnis;
        } else if (this.naechster != null) {
            return this.naechster.umrechnen(variante, betrag); //ChainOfResponsibility-Pattern
        } else {
            throw new KeinPassenderUmrechnerException();
            //TODO: Message als Parameter mitgeben
        }
    }


    /**
     * Hängt einen Observer an den Währungsrechner.
     * @param observer Observer-Element.
     */
    public void addObserver(IObserver observer){observerListe.add(observer);} //Observer-Pattern

    /**
     * @return Das nächste Kettenelement in der Chain-Of-Responsibility.
     */
    public IUmrechnen getNaechster(){return this.naechster;} //ChainOfResponsibility-Pattern

    /**
     * Setzt das nächste Kettenelement in der Chain-Of-Responsibility.
     * @param naechster Der nächste Umrechner.
     */
    public void setNaechster(IUmrechnen naechster){
        if (naechster != null){
            this.naechster.setNaechster(naechster);
        } else {
            this.naechster = naechster;
        }
    }

    /**
     * Benachrichtigt die Observer
     * @param betrag Der Startbetrag
     * @param variante Die Umrechnungsvariante
     * @param ergebnis Das Umrechnungsergebnis
     */
    public void alleBeobachterBenachrichtigen(double betrag, String variante, double ergebnis) {
        for (IObserver o : observerListe) {
            o.benachrichtigen(betrag, ergebnis, variante);
        }
    }

}
