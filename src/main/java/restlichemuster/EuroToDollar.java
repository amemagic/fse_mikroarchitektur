package restlichemuster;

/**
 * Euro -> Dollar - Umrechner
 */
public class EuroToDollar extends WR {

    /**
     * Builder-Pattern
     */
    public static class EuroToDollarBuilder {

        private double faktor;
        // private String zustaendigkeit;

        private IUmrechnen naechster;

        public EuroToDollarBuilder mitNaechster(IUmrechnen naechster){
            this.naechster = naechster;
            return this;
        }

        public EuroToDollarBuilder mitFaktor(double faktor){
            this.faktor = faktor;
            return this;
        }

        /*public EuroToDollarBuilder mitZustaendigkeit(String zustaendigkeit){
            this.zustaendigkeit = zustaendigkeit;
            return this;
        }*/

        /**
         * Initialisiert ein EuroToDollar-Objekt mit den
         * Werten aus dem Builder
         * @return EuroToDollar-Objekt
         */
        public EuroToDollar build (){
            EuroToDollar etd = new EuroToDollar(this.naechster);
            etd.setFaktor(this.faktor);
            return etd;
        }

    }

    private double faktor = 1.06; // Euro -> Dollar Kurs
    private String zustaendigkeit = "eur2dol";

    public EuroToDollar() {super();}
    public EuroToDollar(IUmrechnen naechster) {super(naechster);}

    public double getFaktor() {
        return this.faktor;
    }

    public boolean zustaendig(String variante) {
        if(variante.equals(this.zustaendigkeit)){
            return true;
        } else {
            return false;
        }
    }

    private void setFaktor(double faktor) {
        this.faktor = faktor;
    }

}
