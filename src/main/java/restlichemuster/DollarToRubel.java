package restlichemuster;

public class DollarToRubel extends WR {

    private double faktor = 93.32;
    private String zustaendigkeit = "dol2rub";

    public DollarToRubel(){super();}
    public DollarToRubel(IUmrechnen naechster) {
        super(naechster);
    }

    public double getFaktor() {
        return this.faktor;
    }

    public boolean zustaendig(String variante) {
        if(variante.equals(this.zustaendigkeit)){
            return true;
        } else {
            return false;
        }
    }

}
