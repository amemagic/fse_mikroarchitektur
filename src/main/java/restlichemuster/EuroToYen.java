package restlichemuster;

/**
 * Euro -> Yen - Umrechner
 */
public class EuroToYen extends WR {

    private double faktor = 158.77; // Euro -> Yen - Kurs
    private String zustaendigkeit = "eur2yen";

    public EuroToYen(){super();}
    public EuroToYen(IUmrechnen naechster) {
        super(naechster);
    }

    public double getFaktor() {
        return this.faktor;
    }

    public boolean zustaendig(String variante) {
        if(variante.equals(this.zustaendigkeit)){
            return true;
        } else {
            return false;
        }
    }

}
