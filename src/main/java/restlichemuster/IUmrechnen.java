package restlichemuster;

import observer.IObserver;

/**
 * Legt Funktionalitäten für Umrechner fest.
 * Verwendete Patterns:
 * Chain-Of-Responsibility
 * Observer-Pattern
 * Template-Pattern
 */
public interface IUmrechnen {

    double umrechnen(String variante, double betrag) throws KeinPassenderUmrechnerException;

    double getFaktor();

    boolean zustaendig(String variante); // COR

    void setNaechster(IUmrechnen naechster); // COR

    void addObserver(IObserver observer); // Observer-Pattern.

}
